<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Logo</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Recipes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-target="#recipeModalCenter" data-toggle="modal">Add Recipe</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#category" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Appetizers & Snacks</a>
          <a class="dropdown-item" href="#">Brunch</a>
          <a class="dropdown-item" href="#">Dinners</a>
        </div>
      </li>
      @guest
        <!--   <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li> -->
<!--           <li class="nav-item">
              <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
          </li> -->
      @else
          <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  {{ Auth::user()->name }} <span class="caret"></span>
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>
                  <a class="dropdown-item" href="/profile">
                    Account Profile
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
              </div>
          </li>
      @endguest
    </ul>
  </div>
</nav>


<div class="modal fade" id="recipeModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div>
            <h5>New Recipe</h5>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            @csrf
          <div class="form-group">
            <label for="formGroupName">Name:</label>
            <input type="text" class="form-control" id="formGroupName" name="recipename" placeholder="Recipe Name">
          </div>
          <div class="form-group">
            <label for="formGroupAuthor">Posted by:</label>
            <input type="text" class="form-control" id="formGroupAuthor" name="postedby" placeholder="Author">
          </div> 
          <div class="form-group">
            <label for="formGroupIngredients">Ingredients:</label>
            <input type="text" class="form-control" id="formGroupIngredients" name="ingredients" placeholder="Ingredients">
            <button class="btn btn-info my-2" id="addIngredient" type="button">Add Ingredient</button>
            <input type="hidden" name="ingredients1">
            <ul id="list_ingredients"></ul>
          </div>
           <div class="form-group">
            <select name="category_id" id="category">
              @foreach(App\Category::all() as $category)
              <option value="{{$category->id}}">{{$category->name}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="formGroupProcedure">Procedure:</label>
            <input type="text" class="form-control" id="procedure" name="procedure" placeholder="Procedure">
          </div>
          <div class="form-group">
          <form enctype="multipart/form-data" action="/recipes/create" method="post" id="addRecipeForm">
            <label for="formGroupImage">Image:</label>
            <input type="file" class="form-control" name="image" id="image">
          </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="save">Save changes</button>
        </div>
      </div>
    </div>
  </div>