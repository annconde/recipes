@extends('template')

@section('content')
<div class="container-fluid">
    <div class="row mx-0 px-0">
        <div class="col-12" id="header">
            <!-- <div class="overlay"></div> -->
        	<div class="row">
                <div class="col-12">
                    <div class="white-text" id="header-text">
                        <h2 class="special-font">Brunchworthy</h2>
                        <h1>
                            <span class="text title">Breakfast and Brunch Recipes</span>
                        </h1>
                        <span class="text subtitle">Start your day with an easy pancake or omelet breakfast. Or plan a show stopping brunch with quiches, waffles, casseroles, and more!</span>
                    </div>   
                </div>
            </div>
            <div class="row">
                <div>
                    
                </div>
            </div>
            <div class="row">
                @foreach($recipes as $recipe)
                <div class="card col-7 mr-5 ml-2 mt-3">
                    <div class="row">
                        <div class="col-6">
                            <img src="/{{$recipe->image}}" alt="Recipe Photo">
                            <h3>{{$recipe->name}}</h3>
                            <p>{{$recipe->rating}}</p>
                        </div>
                    <div class="card-body col-6">
                            <h3>Ingredients</h3>
                            <p class="card-text">{!! $recipe->ingredients !!}</p> 
                            <h3>Procedure</h3>       
                            <p class="card-text">{!! $recipe->procedure !!}</p>        
                    </div> <!-- end of col -->
                    </div> <!-- end of row -->
                </div> <!-- end of card -->

                <div class="card col-4 ml-1 mt-3">

                <img src="/{{$recipe->image}}" alt="Recipe Photo">

                <div class="card-body">
                <p class="card-text">The sun disappeared behind the horizon today. Experts claim it could be gone for good!</p>
                </div>

                </div>

                 @endforeach
            </div> <!-- end of row -->
      
            
        </div>
    </div>
</div>

@endsection
