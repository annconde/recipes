@extends('template')

@section('content')
<div class="container-fluid">
	<div class="row">
		<h2>Recipe of the Day</h2>
		<div class="card-body col-9">
			<div class="row">
				<div class="col-4">
					<img src="/{{$recipe->image}}" alt="Recipe Photo">
	                <h3>{{$recipe->name}}</h3>
	                <p>{{$recipe->rating}}</p>
	                <hr>
	                <h4>{{$recipe->author}}</h4>
				</div>
			</div>
		</div>
		<div class="card-body col-3">
			<h2>Most Popular Recipe</h2>
			<div>
				<img src="/{{$recipe->image}}" alt="Recipe Photo">
                <h3>{{$recipe->name}}</h3>
                <h4>{{$recipe->author}}</h4>
                <p>{{$recipe->rating}}</p>
			</div>
		</div>
	</div>
</div>

@endsection
