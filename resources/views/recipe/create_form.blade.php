@extends('template')

@section('title', 'Recipe')

@section('content')
	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#recipeModalCenter">Add New Recipe
	</button>

	<!-- Modal -->
	<div class="modal fade" id="recipeModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	      	<div>
	      		<h5>New Recipe</h5>

	      		<form enctype="multipart/form-data" action="/recipes/create" method="post">
				  <div class="form-group">
				    <label for="formGroupName">Name:</label>
				    <input type="text" class="form-control" id="formGroupName" placeholder="Recipe Name">
				  </div>
				  <div class="form-group">
				    <label for="formGroupAuthor">Posted by:</label>
				    <input type="text" class="form-control" id="formGroupAuthor" placeholder="Author">
				  </div> 
				  <div class="form-group">
				    <label for="formGroupIngredients">Ingredients:</label>
				    <input type="text" class="form-control" id="formGroupAuthor" placeholder="Ingredients">
				  </div> 
				  <div class="form-group">
				    <label for="formGroupProcedure">Procedure:</label>
				    <input type="text" class="form-control" id="formGroupAuthor" placeholder="Procedure">
				  </div>
				  <div class="form-group">
				    <label for="formGroupImage">Image:</label>
				    <input type="file" class="form-control" id="formGroupImage">
				  </div>
				</form>
	      	</div>
	        
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        ...
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>

@endsection
