<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/recipes', function () {
	return view('recipes');
});


Route::get('/recipes', 'RecipeController@index');
Route::post('/recipes/create', 'RecipeController@create');
Route::post('/recipes', 'RecipeController@store');
// Route::post('/recipes/create_image', 'RecipeController@create_image');
// Route::get('/recipes', 'RecipeController@show');
Route::post('/recipes', 'RecipeController@edit');
Route::post('/recipes', 'RecipeController@update');
Route::post('/recipes', 'RecipeController@destroy');



Auth::routes();