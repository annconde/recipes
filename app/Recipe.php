<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    function user(){
        return $this->belongsTo('App\User');
    }

    function reviews(){
        return $this->belongsToMany('App\User', 'reviews')
                    ->withTimestamps();
    }
}
