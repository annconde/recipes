<?php

namespace App\Http\Controllers;

use App\Recipe;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $recipe = new Recipe;
        $recipe->name = $request->name;
        $recipe->author = $request->author;
        $recipe->ingredients = $request->ingredients;
        $recipe->image = '';
        $recipe->procedure = $request->procedure;
        $recipe->category_id = $request->category;
        $recipe->rating = 0;
        $recipe->save();

        if($request->hasFile('image')) {
            $extension = $request->image->getClientOriginalExtension();
            $request->image->storeAs('public/images/recipes/', "$recipe->id.$extension");
            $recipe->image = "storage/images/recipes/$recipe->id.$extension";
            $recipe->save();

            // dd($request->image);
        }

        $recipes = Recipe::all();
       
        return redirect('/home', compact($recipes));
    }

    // public function create_image(Request $request)
    // {

    //      if($request->hasFile('image')) {
    //         $extension = $request->image->getClientOriginalExtension();
    //         $request->image->storeAs('public/images/recipes/', "$recipe->id.$extension");
    //         $recipe->image = "storage/images/recipes/$recipe->id.$extension";
    //         $recipe->save();

    //         dd($request->image);
    //     }
        
    // }

    public function store(Request $request)
    {
        // $recipes = Recipe::all();
        // dd($recipes);
    }

    public function show(Recipe $recipe)
    {
        
    }

   
    public function edit(Recipe $recipe)
    {
        //
    }

    
    public function update(Request $request, Recipe $recipe)
    {
        //
    }

  
    public function destroy(Recipe $recipe)
    {
        //
    }
}
