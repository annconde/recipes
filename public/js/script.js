$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('button#save').click(function(){
    let name = $('input#formGroupName').val();
    let author = $('input#formGroupAuthor').val();
    // let ingredients= $('ul#list_ingredients').html();
    let category= $('select#category').val();
    // let procedure = $('input#procedure').val();
    
    $.ajax({
      url: '/recipes/create',
      method: 'post',
      data: {
        name:name,
        author:author,
        category:category
      },
      success:function(data){
        location.reload();
      }
    })
})

$('#addIngredient').click( ()=>{
    let val = $('#formGroupIngredients').val();
    console.log(val)
    $('#list_ingredients').append('<li>'+val+'</li>');
    $('input[name="ingredients"]').val(val);
    if($('input[name="ingredients"]').val() >= 1){
      $('input[name="ingredients"]').val(+val);
    } else {
      $('input[name="ingredients"]').val(val);
    }

    // $html = $('#list_ingredients').append('<li>'+val+'</li>');
    // $doc = new DOMDocument();
    // $doc->loadHTML($html);
    // $liList = $doc->getElementsByTagName('li');
    // $liValues = array();
    // foreach ($liList as $li) {
    //     $liValues[] = $li->nodeValue;
    //   }

    //   var_dump($liValues);

  });

  // $('#save').click( ()=>{
  //   $('#addRecipeForm').submit();
  // });


